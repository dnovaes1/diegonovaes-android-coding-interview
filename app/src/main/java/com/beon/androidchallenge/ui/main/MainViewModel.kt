package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import com.beon.androidchallenge.domain.model.FactState
import com.beon.androidchallenge.domain.model.FactStateData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private var currentFactState = FactStateData(
        data = null,
        error = null,
        state = FactState.IDLE
    )

    private val _currentFact = MutableLiveData<FactStateData>(currentFactState)
    val currentFact = _currentFact

    fun searchNumberFact(number: String) {
        if (number.isEmpty()) {
            currentFact.postValue(null)
            return
        }

        postLoadingFactNumber()

        viewModelScope.launch(Dispatchers.IO) {
            delay(3000)
            FactRepository.getInstance().getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
                override fun onResponse(response: Fact) {
                    currentFactState = currentFactState.copy(
                        data = response,
                        state = FactState.DONE,
                        error = null
                    )
                    currentFact.postValue(currentFactState)
                }

                override fun onError() {
                    currentFactState = currentFactState.copy(
                        state = FactState.DONE,
                        error = "Application unable to get fact about '$number'"
                    )
                    currentFact.postValue(currentFactState)
                }

            })
        }
    }

    private fun postLoadingFactNumber() {
        currentFactState = currentFactState.copy(
            state = FactState.PROCESSING,
        )
        currentFact.postValue(currentFactState)
    }
}