package com.beon.androidchallenge.domain.model

import com.google.gson.annotations.SerializedName

data class FactStateData(
    val data: Fact? = null,
    val state: FactState,
    val error: String?,
)

enum class FactState {
    IDLE,
    PROCESSING,
    DONE
}

fun FactState.isProcessing() =
    this == FactState.PROCESSING

fun FactState.isDone() =
    this == FactState.DONE

class Fact {
    @SerializedName("text")
    var text: String? = null
    @SerializedName("number")
    var number: Long? = null
    @SerializedName("found")
    var found: Boolean? = null
    @SerializedName("type")
    var type: String? = null
}